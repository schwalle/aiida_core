## To install these requirements, run
## pip install -U -r requirements.txt
## (the -U option also upgrades packages; from the second time on,
## just run
## pip install -r requirements.txt
##
## NOTE: before running the command above, you need to install a recent version
## of pip from the website, and then possibly install/upgrade setuptools using
## sudo pip install --upgrade setuptools

## Django
django==1.7.4

## For the UUID field
django_extensions==1.5

## For timezone support
pytz==2014.10

## For the daemon
django-celery == 3.1.16
celery==3.1.17
kombu==3.0.24
billiard==3.3.0.19
amqp==1.4.9
anyjson==0.3.3
six==1.9
supervisor==3.1.3
meld3==1.0.0

## For postgreSQL
psycopg2==2.6

## ONLY if using mysql? 
#sudo easy_install mysql-python
#sudo apt-get install python-mysqldb on ubuntu 

## To have a decent recent version of sqlite
## Note that django uses pysqlite instead of the system-provided
## sqlite3, if pysqlite is available: django/db/backends/sqlite3/base.py
## This is because we need recursive triggers in sqlite, present
## only after sqlite 3.6.18
## Commented for rtd
##pysqlite==2.6.3

## Paramiko, for ssh connections
paramiko==1.15.2
ecdsa==0.13
pycrypto==2.6.1

## NumPy, mainly for the ArrayData objects but sometimes
## used also for arrays in other parts of the code
numpy

## Tastypie, for the REST API interface
django-tastypie==0.12.1
python-dateutil==2.4.0
python-mimeparse==0.1.4

# SQLAlchemy support
SQLAlchemy==1.0.12
SQLAlchemy-Utils==0.31.2
ultrajson==1.35

# For workflows
enum34==1.1.2
git+https://bitbucket.org/aiida_team/plum.git@v0.4.3#egg=plum
voluptuous==0.8.11

# For the QueryBuilder in Django when testing:
aldjemy
# Password hashing
passlib

# Email validation for verdi install
validate_email

# For composable command line options
click==6.6

# Pretty-print tables
tabulate==0.7.5

# To get ASCII trees, may make this optional later
ete3==3.0.0b35
